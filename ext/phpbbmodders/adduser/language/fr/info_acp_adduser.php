<?php
/**
 *
 * @package phpBB Extension - Add User
 * @author RMcGirr83  (Rich McGirr) rmcgirr83@rmcgirr83.org
 * @copyright (c) 2014 phpbbmodders.net
 * @license GNU General Public License, version 2 (GPL-2.0)
 * @2016-12-04 : French translation by Tlem (tlem@tuxolem.fr)
 *
 */

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
* DO NOT CHANGE
*/
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, array(
	// ACP Module
	'ACP_ADD_USER'				=> 'Ajouter un Utilisateur',
	'DIR_NOT_EXIST'				=> 'La langue que vous avez choisi %s ne possède pas les fichiers nécessaires à l’extension. Veuillez les traduire et les télécharger dans le répertoire %s de l’extension pour utiliser l’extension.',
	'ACP_ACCOUNT_ADDED'			=> 'Le compte utilisateur a été créé. L’utilisateur peut maintenant se connecter avec le nom d’utilisateur et le mot de passe envoyé à l’adresse e-mail que vous avez fournie.',
	'ACP_ACCOUNT_INACTIVE'		=> 'Le compte utilisateur a été créé. Cependant, les paramètres du forum exigent que l’utilisateur active son compte. <br /> Une clé d’activation a été envoyée à l’adresse e-mail que vous avez fourni à l’utilisateur.',
	'ACP_ACCOUNT_INACTIVE_ADMIN'=> 'Le compte a été créé. Cependant, les paramètres du forum nécessitent l’activation du compte par un administrateur. <br /> Un email a été envoyé aux Administrateurs et l’utilisateur sera informé de l’activation de son compte',
	'ACP_ADMIN_ACTIVATE'		=> 'Un courrier électronique sera envoyé à un administrateur pour l’activation du compte. Sinon, vous pouvez cocher la case d’activation du compte ci-dessous pour activer instantanément le compte une fois créé. L’utilisateur recevra un courriel contenant les détails de connexion au compte.',
	'ACP_EMAIL_ACTIVATE'		=> 'Une fois le compte créé, l’utilisateur recevra un courriel contenant un lien d’activation pour activer le compte.',
	'ACP_INSTANT_ACTIVATE'		=> 'Le compte sera activé instantanément. L’utilisateur recevra un email avec les détails de connexion au compte.',

	'ADD_USER'					=> 'Ajouter un utilisateur',
	'ADD_USER_EXPLAIN'			=> 'Créez un nouveau compte d’utilisateur. Si vos paramètres d’activation sont uniquement pour l’activation d’Admin, vous avez la possibilité d’activer instantanément l’utilisateur.',
	'MOD_VERSION'				=> 'Version %s',
	'ADMIN_ACTIVATE'			=> 'Activer un compte utilisateur',
	'CONFIRM_PASSWORD'			=> 'Confirmer le mot de passe',
	'EDIT_USER_GROUPS'			=> '%sClic ici pour éditer les groupes pour cet utilisateur%s',
	'GROUP_ADD'					=> 'Une sélection ici ajoutera l’utilisateur au groupe sélectionné ainsi qu’au groupe des utilisateurs enregistrés.',
	'GROUP_DEFAULT_EXPLAIN'		=> 'Cochez cette case pour que le groupe sélectionné ci-dessus soit la valeur par défaut des utilisateurs.',
	'CONTINUE_EDIT_USER'		=> '%1$sCliquez ici pour gérer le profile de %2$s%3$s', // e.g.: Click here to edit Joe’s profile.
	'PASSWORD_EXPLAIN'			=> 'Si laissé à blanc, un mot de passe sera généré automatiquement.',
	'ENABLE_NEWUSER'			=> 'Activer le nouvel utilisateur',
	'ENABLE_NEWUSER_EXPLAIN'	=> 'Si défini comme oui, l’utilisateur sera ajouté au groupe d’utilisateurs nouvellement enregistré',
	// ACP Logs
	'LOG_USER_ADDED'			=> '<strong>Nouvel utilisateur créé</strong><br />» %s',
));
