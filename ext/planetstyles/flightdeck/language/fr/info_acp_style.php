<?php

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_STYLE_SETTINGS_TITLE'		=> 'Flight Deck (Panneau de Contr&ocirc;le)',
	'ACP_STYLE_SETTINGS_SETTINGS'	=> 'R&eacute;glages du th&egrave;me',
));
