<?php

/**
 *
 * @package phpBB Extension - mChat
 * @copyright (c) 2016 dmzx - http://www.dmzx-web.net
 * @copyright (c) 2016 kasimi - https://kasimi.net
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 * @French Translation - Tlem - tlem at tuxolem dot fr
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters for use
// ’ » “ ” …

$lang = array_merge($lang, array(
	'MCHAT_PREFERENCES'				=> 'Préférences mChat',
	'MCHAT_NO_SETTINGS'				=> 'Vous n’êtes pas autorisé à personnaliser les paramètres.',

	'MCHAT_INDEX'					=> 'Affichage sur la page d’index',
	'MCHAT_SOUND'					=> 'Activer le son',
	'MCHAT_WHOIS_INDEX'				=> 'Afficher <em>"Qui est dans le chat"</em> sous le chat',
	'MCHAT_STATS_INDEX'				=> 'Afficher <em>"Qui est dans le chat"</em> dans la section stats',
	'MCHAT_STATS_INDEX_EXPLAIN'		=> 'Affiche les utilisateurs dans le chat sous la section <em>"Qui est en ligne"</em> de la page d’index.',
	'MCHAT_AVATARS'					=> 'Affiche les avatars',
	'MCHAT_CAPITAL_LETTER'			=> 'Met en majuscule la première lettre de vos messages',
	'MCHAT_CHAT_AREA'				=> 'Type d’entrée',
	'MCHAT_INPUT_AREA'				=> 'Champ de saisie',
	'MCHAT_TEXT_AREA'				=> 'Zone de texte',
	'MCHAT_POSTS'					=> 'Afficher les nouveaux messages (actuellement désactivé, peut être activé dans la section Paramètres globaux mChat de l’ACP)',
	'MCHAT_DISPLAY_CHARACTER_COUNT'	=> 'Afficher le nombre de caractères lors de la saisie d’un message',
	'MCHAT_RELATIVE_TIME'			=> 'Afficher le temps relatif pour les nouveaux messages',
	'MCHAT_RELATIVE_TIME_EXPLAIN'	=> 'Afficher “A l’instant”, “Il y a 1 minute” et ainsi de suite pour chaque message. Mettre à <em>Non</em> pour toujours afficher la date complète.',
	'MCHAT_PAUSE_ON_INPUT'			=> 'Pause sur l’entrée',
	'MCHAT_PAUSE_ON_INPUT_EXPLAIN'	=> 'Ne pas mettre à jour mChat lors de la saisie d’un message',
	'MCHAT_MESSAGE_TOP'				=> 'Emplacement des nouveaux messages du chat',
	'MCHAT_MESSAGE_TOP_EXPLAIN'		=> 'Les nouveaux messages apparaissent en haut ou en bas du chat.',
	'MCHAT_LOCATION'				=> 'Emplacement sur la page d’index',
	'MCHAT_BOTTOM'					=> 'En Bas',
	'MCHAT_TOP'						=> 'En Haut',

	'MCHAT_POSTS_TOPIC'				=> 'Afficher les nouveaux sujets',
	'MCHAT_POSTS_REPLY'				=> 'Afficher les nouvelles réponses',
	'MCHAT_POSTS_EDIT'				=> 'Afficher les messages modifiés',
	'MCHAT_POSTS_QUOTE'				=> 'Afficher les messages cités',
	'MCHAT_POSTS_LOGIN'				=> 'Afficher les connexions utilisateur',

	'MCHAT_DATE_FORMAT'				=> 'Format de date',
	'MCHAT_DATE_FORMAT_EXPLAIN'		=> 'La syntaxe utilisée est identique à celle de la fonction PHP <a href="http://www.php.net/date">date()</a>.',
	'MCHAT_CUSTOM_DATEFORMAT'		=> 'Personnalisé …',
));
