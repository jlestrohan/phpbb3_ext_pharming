<?php

/**
 *
 * @package phpBB Extension - mChat
 * @copyright (c) 2016 dmzx - http://www.dmzx-web.net
 * @copyright (c) 2016 kasimi - https://kasimi.net
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 * @French Translation - Tlem - tlem at tuxolem dot fr
*
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters for use
// ’ » “ ” …

$lang = array_merge($lang, array(
	'MCHAT_TITLE'					=> 'mChat',
	'MCHAT_TITLE_COUNT'				=> 'mChat [<strong>%1$d</strong>]',

	// Who is chatting
	'MCHAT_WHO_IS_CHATTING'			=> 'Qui est dans le chat',
	'MCHAT_ONLINE_USERS_TOTAL'		=> array(
		0 => 'Personne dans le chat',
		1 => '<strong>%1$d</strong> utilisateur dans le chat',
		2 => '<strong>%1$d</strong> utilisateurs dans le chat',
	),
	'MCHAT_ONLINE_EXPLAIN'			=> 'basé sur les utilisateurs actifs au cours des dernieres %1$s',
	'MCHAT_HOURS'					=> array(
		1 => '%1$d heure',
		2 => '%1$d heures',
	),
	'MCHAT_MINUTES'					=> array(
		1 => '%1$d minute',
		2 => '%1$d minutes',
	),
	'MCHAT_SECONDS'					=> array(
		1 => '%1$d seconde',
		2 => '%1$d secondes',
	),

	// Post notification messages (%1$s is replaced with a link to the new/edited post, %2$s is replaced with a link to the forum)
	'MCHAT_NEW_POST'				=> 'A publié un nouveau sujet: %1$s dans %2$s',
	'MCHAT_NEW_POST_DELETED'		=> 'A publié un nouveau sujet qui a été supprimé',
	'MCHAT_NEW_REPLY'				=> 'A publié une réponse: %1$s dans %2$s',
	'MCHAT_NEW_REPLY_DELETED'		=> 'A publié une réponse qui a été supprimée',
	'MCHAT_NEW_QUOTE'				=> 'A répondu en citant: %1$s dans %2$s',
	'MCHAT_NEW_QUOTE_DELETED'		=> 'A publié une réponse qui a été supprimée',
	'MCHAT_NEW_EDIT'				=> 'A édité un message: %1$s dans %2$s',
	'MCHAT_NEW_EDIT_DELETED'		=> 'A édité un message qui a été supprimé',
	'MCHAT_NEW_LOGIN'				=> 'vient de se connecter',
));
