<?php

/**
 *
 * @package phpBB Extension - mChat
 * @copyright (c) 2016 dmzx - http://www.dmzx-web.net
 * @copyright (c) 2016 kasimi - https://kasimi.net
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 * @French Translation - Tlem - tlem at tuxolem dot fr
*
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters for use
// ’ » “ ” …

$lang = array_merge($lang, array(
	'ACL_U_MCHAT_USE'						=> 'Peut utiliser mChat',
	'ACL_U_MCHAT_VIEW'						=> 'Peut voir mChat',
	'ACL_U_MCHAT_EDIT'						=> 'Peut modifier ses propres messages',
	'ACL_U_MCHAT_DELETE'					=> 'Peut supprimer ses propres messages',
	'ACL_U_MCHAT_MODERATOR_EDIT'			=> 'Peut modifier les messages de quiconque',
	'ACL_U_MCHAT_MODERATOR_DELETE'			=> 'Peut supprimer les messages de quiconque',
	'ACL_U_MCHAT_IP'						=> 'Peut voir les adresses IP',
	'ACL_U_MCHAT_PM'						=> 'Peut utiliser les messages privés',
	'ACL_U_MCHAT_LIKE'						=> 'Peut Liker les messages',
	'ACL_U_MCHAT_QUOTE'						=> 'Peut citer des messages',
	'ACL_U_MCHAT_FLOOD_IGNORE'				=> 'Peut ignorer la limite de flood',
	'ACL_U_MCHAT_ARCHIVE'					=> 'Peut voir les archives',
	'ACL_U_MCHAT_BBCODE'					=> 'Peut utiliser les BBCodes',
	'ACL_U_MCHAT_SMILIES'					=> 'Peut utiliser les smilies',
	'ACL_U_MCHAT_URLS'						=> 'Peut publier des des liens',

	'ACL_U_MCHAT_AVATARS'					=> 'Peut personnaliser <em>Afficher les avatars</em>',
	'ACL_U_MCHAT_CAPITAL_LETTER'			=> 'Peut personnaliser <em>Première lettre en majuscule</em>',
	'ACL_U_MCHAT_CHARACTER_COUNT'			=> 'Peut personnaliser <em>Afficher le nombre de caractères</em>',
	'ACL_U_MCHAT_DATE'						=> 'Peut personnaliser <em>Format de date</em>',
	'ACL_U_MCHAT_INDEX'						=> 'Peut personnaliser <em>Afficher sur l’index</em>',
	'ACL_U_MCHAT_INPUT_AREA'				=> 'Peut personnaliser <em>Type d’entrée</em>',
	'ACL_U_MCHAT_LOCATION'					=> 'Peut personnaliser <em>Emplacement de mChat sur la page d’index</em>',
	'ACL_U_MCHAT_MESSAGE_TOP'				=> 'Peut personnaliser <em>Emplacement des nouveaux messages du chat</em>',
	'ACL_U_MCHAT_PAUSE_ON_INPUT'			=> 'Peut personnaliser <em>Pause sur l’entrée</em>',
	'ACL_U_MCHAT_POSTS'						=> 'Peut personnaliser <em>Afficher les nouveaux messages</em>',
	'ACL_U_MCHAT_RELATIVE_TIME'				=> 'Peut personnaliser <em>Afficher le temps relatif</em>',
	'ACL_U_MCHAT_SOUND'						=> 'Peut personnaliser <em>Jouer les sons</em>',
	'ACL_U_MCHAT_WHOIS_INDEX'				=> 'Peut personnaliser <em>Afficher "qui est dans le chat" sous le chat</em>',
	'ACL_U_MCHAT_STATS_INDEX'				=> 'Peut personnaliser <em>Afficher "qui est dans le chat" dans la section stats</em>',

	'ACL_A_MCHAT'							=> 'Peut gérer les paramètres mChat',
));
