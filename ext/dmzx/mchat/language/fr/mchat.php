<?php

/**
 *
 * @package phpBB Extension - mChat
 * @copyright (c) 2016 dmzx - http://www.dmzx-web.net
 * @copyright (c) 2016 kasimi - https://kasimi.net
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 * @French Translation - Tlem - tlem at tuxolem dot fr
*
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters for use
// ’ » “ ” …

$lang = array_merge($lang, array(
	'MCHAT_ADD'						=> 'Envoyer',
	'MCHAT_ARCHIVE'					=> 'Archives',
	'MCHAT_ARCHIVE_PAGE'			=> 'Archives mChat',
	'MCHAT_CUSTOM_PAGE'				=> 'mChat',
	'MCHAT_BBCODES'					=> 'BBCodes',
	'MCHAT_CUSTOM_BBCODES'			=> 'BBCodes Personnalisés',
	'MCHAT_DELCONFIRM'				=> 'Etes-vous sur de vouloir supprimer ce message ?',
	'MCHAT_EDIT'					=> 'Modifier',
	'MCHAT_EDITINFO'				=> 'Modifier le message ci-dessous.',
	'MCHAT_NEW_CHAT'				=> 'Nouveau message !',
	'MCHAT_SEND_PM'					=> 'Envoyer un message privé',
	'MCHAT_LIKE'					=> 'Liker ce message',
	'MCHAT_LIKES'					=> 'Aime ce message',
	'MCHAT_FLOOD'					=> 'Vous devez attendre avant de pouvoir envoyer un nouveau message.',
	'MCHAT_FOE'						=> 'Ce message a été écrit par <strong>%1$s</strong> qui est actuellement dans votre liste noire.',
	'MCHAT_RULES'					=> 'Règles',
	'MCHAT_WHOIS_USER'				=> 'IP whois pour %1$s',
	'MCHAT_MESS_LONG'				=> 'Votre message est trop long. Veuillez le limiter à %1$d caractères.',
	'MCHAT_NO_CUSTOM_PAGE'			=> 'La page personnalisée mChat n’est pas activée en ce moment.',
	'MCHAT_NO_RULES'				=> 'La page des règles mChat n’est pas activée pour le moment.',
	'MCHAT_NOACCESS_ARCHIVE'		=> 'Vous n’avez pas l’autorisation de consulter les archives.',
	'MCHAT_NOJAVASCRIPT'			=> 'Veuillez activer JavaScript pour utiliser mChat.',
	'MCHAT_NOMESSAGE'				=> 'Aucun messages',
	'MCHAT_NOMESSAGEINPUT'			=> 'Vous n’avez pas entré de message',
	'MCHAT_MESSAGE_DELETED'			=> 'Ce message a été supprimé.',
	'MCHAT_OK'						=> 'OK',
	'MCHAT_PAUSE'					=> 'En pause',
	'MCHAT_PERMISSIONS'				=> 'Modifier les autorisations de l’utilisateur',
	'MCHAT_REFRESHING'				=> 'Actualisation …',
	'MCHAT_REFRESH_NO'				=> 'Mise à jour désactivée',
	'MCHAT_REFRESH_YES'				=> 'Mise à jour toutes les <strong>%1$d</strong> secondes',
	'MCHAT_RESPOND'					=> 'Répondre à l’utilisateur',
	'MCHAT_SESSION_ENDS'			=> 'La session de chat se termine dans %1$s',
	'MCHAT_SESSION_OUT'				=> 'La session de chat a expiré',
	'MCHAT_SMILES'					=> 'Smilies',
	'MCHAT_TOTALMESSAGES'			=> 'Total des messages: <strong>%1$d</strong>',
	'MCHAT_USESOUND'				=> 'Jouer un son',
	'MCHAT_COLLAPSE_TITLE'			=> 'Activer la visibilité du mChat',
	'MCHAT_WHO_IS_REFRESH_EXPLAIN'	=> 'Actualisation toutes les <strong>%1$d</strong> secondes',
	'MCHAT_MINUTES_AGO'				=> array(
		0 => 'à l’instant',
		1 => 'il y à %1$d minute',
		2 => 'il y à %1$d minutes',
	),

	// These messages are formatted with JavaScript, hence {} and no %d
	'MCHAT_CHARACTER_COUNT'			=> '<strong>{current}</strong> charactères',
	'MCHAT_CHARACTER_COUNT_LIMIT'	=> '<strong>{current}</strong> sur {max} charactères',
	'MCHAT_SESSION_ENDS_JS'			=> 'La session de chat se termine dans {timeleft}',
	'MCHAT_MENTION'					=> ' @{username} ',

	// Custom translations for administrators
	'MCHAT_RULES_MESSAGE'			=> '',
	'MCHAT_STATIC_MESSAGE'			=> '',
));
