<?php

/**
*
*
* @package - National Flags language
* @copyright (c) RMcGirr83
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
* French translation by Pierre Duhem
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
// Some characters you may want to copy&paste:
// ’ » “ ” …

$lang = array_merge($lang, array(
	//Module and page titles
	'ACP_CAT_FLAGS'						=> 'Drapeaux nationaux',
	'ACP_FLAGS'							=> 'Drapeaux nationaux',
	'ACP_FLAGS_EXPLAIN'					=> 'Vous pouvez ci-dessous ajouter, éditer et supprimer les différents drapeaux.',
	'ACP_NO_UPLOAD'						=> '<br /><strong>Si vous voulez utiliser des images, vous devez les télécharger dans le dossier ext/rmcgirr83/nationalflags/flags avant d\'ajouter le nouveau drapeau. Le nom du fichier doit être en minuscules, par exemple uk.gif</strong>',
	'ACP_FLAGS_DONATE'					=> 'Si vous aimez cette extension, vous pouvez faire un <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=S4UTZ9YNKEDDN" onclick="window.open(this.href); return false;"><strong>don</strong></a>',
	'ACP_FLAG_USERS'					=> 'Nombre d\'utilisateurs',

	//Add/Edit Flags
	'FLAG_EDIT'							=> 'Édition d\'un drapeau',
	'FLAG_NAME'							=> 'Nom du drapeau',
	'FLAG_NAME_EXPLAIN'					=> 'Nom du drapeau. Le titre est affiché sous la forme ci-contre.',
	'FLAG_IMG'							=> 'Nom de l\'image',
	'FLAG_IMG_EXPLAIN'					=> 'Nom du fichier contenant l\'image. Les nouvelles images doivent être téléchargées dans le dossier ext/rmcgirr83/nationalflags/flags.',
	'FLAG_IMAGE'						=> 'Image de drapeau',
	'FLAG_ADD'							=> 'Ajouter un drapeau',
	'FLAG_UPLOAD'						=> 'Télécharger un drapeau',
	'FLAG_UPLOAD_NOTICE'				=> 'Le téléchargement écrase le fichier s\'il en existe un sous ce nom sur le serveur. Le nom du fichier est sensible à la casse.',
	'FLAG_UPLOAD_NO_OVERWRITE'			=> 'Vous ne pouvez pas écraser un fichier d\'image qui existe sous le même nom avec la même extension que ci-dessous.',
	'FLAG_FOUND'						=> 'Le drapeau a été trouvé',
	'IMAGES_ON_SERVER'					=> 'Noms de fichiers sur le serveur',

	//Settings
	'ACP_FLAG_SETTINGS'					=> 'Paramètres des Drapeaux nationaux',
	'YES_FLAGS'							=> 'Valider les drapeaux',
	'YES_FLAGS_EXPLAIN'					=> 'Validez ou inhibez les Drpaeaux nationaux',
	'FLAGS_VERSION'						=> 'Drapeaux nationaux version',
	'FLAGS_REQUIRED'					=> 'Choix obligatoire',
	'FLAGS_REQUIRED_EXPLAIN'			=> 'Si vous choisissez Oui, vous forcez les nouevaux inscrits et les utilisateurs qui visitent leur profil à choisir un drapeau',
	'FLAGS_DISPLAY_MSG'					=> 'Afficher un message',
	'FLAGS_DISPLAY_MSG_EXPLAIN'			=> 'Si vous choisissez Oui, un message est affiché sur le forum pour inviter l\'utilisateur à choisir un drapeau',
	'FLAGS_NUM_DISPLAY'					=> 'Nombre de drapeaux',
	'FLAGS_NUM_DISPLAY_EXPLAIN'			=> 'Nombre de drapeaux à afficher sur la page d\'index du forum',
	'FLAGS_ON_INDEX'					=> 'Afficher sur l\'index',
	'FLAGS_ON_INDEX_EXPLAIN'			=> 'Affichage d\'un tableau récapitulatif des utilisateurs de drapeaux sur la page d\'index',
	'FLAGS_DISPLAY_TO_GUESTS'			=> 'Afficher les drapeaux aux visiteurs',
	'FLAGS_DISPLAY_TO_GUESTS_EXPLAIN'	=> 'Si vous choisissez Oui, les drapeaux seront visibles pour les visiteurs et les robots',

	//Logs, messages and errors
	'LOG_FLAGS_DELETED'					=> '<strong>Drapeau supprimé</strong><br />» %1$s',
	'LOG_FLAG_EDIT'						=> '<strong>Drapeau mis à jour</strong><br />» %1$s',
	'LOG_FLAG_ADD'						=> '<strong>Nouveau drapeau ajouté</strong><br />» %1$s',
	'MSG_FLAGS_DELETED'					=> 'Le drapeau a été supprimé.',
	'MSG_CONFIRM'						=> '<strong>Êtes-vous certain de vouloir supprimer ce drapeau ?</strong>',
	'MSG_FLAG_CONFIRM_DELETE'			=> array(
		1	=> '<br /><strong>%d</strong> utilisateur utilise ce drapeau et devra en sélectionner un autre si vous le supprimez.',
		2	=> '<br /><strong>%d</strong> utilisateurs utilisent ce drapeau et devront en sélectionner un autre si vous le supprimez.',
	),
	'MSG_FLAG_EDITED'					=> 'Le drapeau a été édité.',
	'MSG_FLAG_ADDED'					=> 'Le nouveau drapeau a été ajouté.',
	'FLAG_ERROR_NO_FLAG_NAME'			=> 'Pas de nom renseigné, la zone est obligatoire.',
	'FLAG_ERROR_NO_FLAG_IMG'			=> 'Pas de nom de fichier renseigné, la zone est obligatoire.',
	'FLAG_ERROR_NOT_EXIST'				=> 'Le drapeau sélectionné n\'existe pas.',
	'FLAG_CONFIG_SAVED'					=> '<strong>Les paramètres des Drapeaux nationaux ont été modifiés</strong>',
	'FLAG_NAME_EXISTS'					=> 'Un drapeau existe déjà sous ce même nom',
	'FLAG_SETTINGS_CHANGED'				=> 'Les paramètres des Drapeaux nationaux ont été modifiés.',
));
