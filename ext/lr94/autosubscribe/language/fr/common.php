<?php

/**
*
* @copyright (c) 2017 Luca Robbiano (lr94)
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'AUTO_SUBSCRIPTION'					=> 'Abonnement automatique pour les auteurs de sujets',
	'AUTO_SUBSCRIPTION_EXPLAIN'			=> 'Si activ� les auteurs seront automatiquement abonn�s a leurs propres sujets',
	
	'AUTO_SUBSCRIPTION_USER'			=> 'Abonnement automatique a mes propres sujets',
));
