<?php

/**
 * @author    MarkusWME <markuswme@pcgamingfreaks.at>
 * @copyright 2017 MarkusWME
 * @license   http://opensource.org/gpl-2.0.php GNU General Public License v2
 * @version   1.0.0
 */

if (!defined('IN_PHPBB'))
{
    exit;
}

if (empty($lang) || !is_array($lang))
{
    $lang = array();
}

// Merge AJAX Registration Check language data to the existing language data
$lang = array_merge($lang, array(
    'PCGF_AJAXREGISTRATIONCHECK_INVALID_QUERY'        => 'La requ&#234;te n&rsquo;est pas valide !',
    'PCGF_AJAXREGISTRATIONCHECK_USERNAME_OK'          => 'Ce nom d&rsquo;utilisateur est disponible.',
    'PCGF_AJAXREGISTRATIONCHECK_EMAIL_INVALID'        => 'Adresse email non valide!',
    'PCGF_AJAXREGISTRATIONCHECK_EMAIL_OK'             => 'Adresse email valide.',
    'PCGF_AJAXREGISTRATIONCHECK_CONFIRM_PASSWORD_OK'  => 'Les mots de passe sont identiques',
    'PCGF_AJAXREGISTRATIONCHECK_PASSWORD_STRENGTH'    => 'Niveau de s&eacute;curit&eacute;',
    'PCGF_AJAXREGISTRATIONCHECK_PASSWORD_VERY_WEAK'   => 'Tr&egrave;s faible',
    'PCGF_AJAXREGISTRATIONCHECK_PASSWORD_WEAK'        => 'Faible',
    'PCGF_AJAXREGISTRATIONCHECK_PASSWORD_NORMAL'      => 'Normal',
    'PCGF_AJAXREGISTRATIONCHECK_PASSWORD_STRONG'      => 'Fort',
    'PCGF_AJAXREGISTRATIONCHECK_PASSWORD_VERY_STRONG' => 'Tr&egrave;s fort',
));
