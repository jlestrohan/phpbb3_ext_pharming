<?php
/**
 *
 * News Scroll. An extension for the phpBB Forum Software package.
 * French translation by tombraid & Galixte (http://www.galixte.com)
 *
 * @copyright (c) 2017 HiFiKabin <https://phpbb.hifikabin.me.uk>
 * @license GNU General Public License, version 2 (GPL-2.0)
 *
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ « » “ ” …
//

$lang = array_merge($lang, array(

    'ACP_NEWSSCROLL'                    => 'Nouvelles défilantes',
    'ACP_NEWSSCROLL_CONFIG'             => 'Gestions des nouvelles',
    'ACP_NEWSSCROLL_CONFIG_EXPLAIN'     => 'Depuis cette page il est possible de modifier les paramètres des nouvelles défilantes.',
    'ACP_NEWSSCROLL_CONFIG_SET'         => 'Paramètres',
    'NEWSSCROLL_CONFIG_SAVED'           => 'Les paramètres des nouvelles défilantes ont été sauvegardés avec succès !',

    'NEWSSCROLL_ACTIVATED'              => 'Activer l’affichage des nouvelles défilantes',
    'NEWSSCROLL_INDEX'                  => 'Afficher les nouvelles uniquement sur la page de l’index du forum',
    'NEWSSCROLL_MEMBER'                 => 'Afficher les nouvelles uniquement aux membres du forum',
    'NEWSSCROLL_SPEED'                  => 'Vitesse de défilement des nouvelles',
    'NEWSSCROLL_SPEED_EXPLAIN'          => 'Permet de saisir la durée en secondes de l’affichage d’un cycle du défilement des nouvelles. Par défaut cette période est définie sur 10 secondes.<br />Plus la valeur est importante et moins le défilement sera rapide, moins la valeur est importante et plus le défilement sera rapide.',

    'NEWSSCROLL_TEXT'                   => 'Texte des nouvelles défilantes',
    'NEWSSCROLL_TEXT_EXPLAIN'           => 'Permet de saisir le texte à aficher dans les nouvelles défilantes.',

    'NEWSSCROLL_PREVIEW'                => 'Aperçu des nouvelles défilantes',
    'NEWSSCROLL_PREVIEW_EXPLAIN'        => 'Permet d’afficher un aperçu de l’affichage des nouvelles défilantes sans avoir à activer l’affichage de celles-ci.',

));
