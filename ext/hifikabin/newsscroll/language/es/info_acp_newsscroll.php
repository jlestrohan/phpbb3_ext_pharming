<?php
/**
*
* @package phpBB Extension - News Scroll
* @copyright (c) 2015 HiFiKabin
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/


/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
   exit;
}

if (empty($lang) || !is_array($lang))
{
   $lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(

    'ACP_NEWSSCROLL'                    => 'Desplazamiento de Noticias',
    'ACP_NEWSSCROLL_CONFIG'             => 'Gestionar Desplazamiento de Noticias',
    'ACP_NEWSSCROLL_CONFIG_EXPLAIN'     => 'Ajustes de Desplazamiento de Noticias',
    'ACP_NEWSSCROLL_CONFIG_SET'         => 'Configuración',
    'NEWSSCROLL_CONFIG_SAVED'           => 'Ajustes de Desplazamiento de Noticias guardados',

    'NEWSSCROLL_ACTIVATED'              => 'Habilitar Desplazamiento de Noticias',
    'NEWSSCROLL_INDEX'                  => 'Mostrar en la página índice solamente',
    'NEWSSCROLL_MEMBER'                 => 'Mostrar a miembros solamente',
    'NEWSSCROLL_SPEED'                  => 'Velocidad del Desplazamiento de Noticias',
    'NEWSSCROLL_SPEED_EXPLAIN'          => 'Este ajuste es en segundos. Por defecto es 10.<br />Un número más alto se desplazará más lentamente y un número más bajo será más rápido.',

    'NEWSSCROLL_TEXT'                   => 'Texto de Desplazamiento de Noticias',
    'NEWSSCROLL_TEXT_EXPLAIN'           => 'Introduzca aquí el texto que desea que aparezca en el Desplazamiento de Noticias.',

    'NEWSSCROLL_PREVIEW'                => 'Vista previa de Desplazamiento de Noticias',
    'NEWSSCROLL_PREVIEW_EXPLAIN'        => 'Al enviar esta página, puede revisar el texto de desplazamiento sin habilitar la extensión de Desplazamiento de Noticias.',
));
