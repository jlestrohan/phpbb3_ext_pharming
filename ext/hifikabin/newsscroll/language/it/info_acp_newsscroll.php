<?php
/**
*
* @package phpBB Extension - News Scroll
* @copyright (c) 2015 HiFiKabin
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
* @Traduzione italiana By dr.house  http://phpbb-store.it
*
*/


/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
   exit;
}

if (empty($lang) || !is_array($lang))
{
   $lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(

    'ACP_NEWSSCROLL'                    => 'Nuovo Scorrimento del testo',
    'ACP_NEWSSCROLL_CONFIG'             => 'Gestione Nuovo Scorrimento del testo',
    'ACP_NEWSSCROLL_CONFIG_EXPLAIN'     => 'Settaggi Nuovo Scorrimento del testo',
    'ACP_NEWSSCROLL_CONFIG_SET'         => 'Configurazione',
    'NEWSSCROLL_CONFIG_SAVED'           => 'Salvataggio Nuovo Scorrimento del testo',

    'NEWSSCROLL_ACTIVATED'              => 'Abilita Nuovo Scorrimento del testo',
    'NEWSSCROLL_INDEX'                  => 'Visualizza solo sulla index page',
    'NEWSSCROLL_MEMBER'                 => 'Visualizza solo agli utenti',
    'NEWSSCROLL_SPEED'                  => 'Settaggio velocità',
    'NEWSSCROLL_SPEED_EXPLAIN'          => 'Questa impostazione è in secondi. Di default è 10.<br />Più alto è impostato il valore minore sarà la velocità di scorrimento e viceversa.',

    'NEWSSCROLL_TEXT'                   => 'Nuovo Scorrimento del testo',
    'NEWSSCROLL_TEXT_EXPLAIN'           => 'Inserisci il testo che vorrai far vedere.',

    'NEWSSCROLL_PREVIEW'                => 'Anteprima del testo creato',
    'NEWSSCROLL_PREVIEW_EXPLAIN'        => 'Inviando questa pagina è possibile esaminare il testo di scorrimento senza abilitare l\'estensione.',

));
