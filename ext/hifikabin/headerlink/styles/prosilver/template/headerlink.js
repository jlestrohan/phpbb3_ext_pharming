$(function () {
	'use strict';

	function isIE() {
		return navigator.userAgent.match(/MSIE \d\.\d+/);
	}

	var $phpbbNavbar = $('.phpbb-navbar');
	var $phpbbMenu = $('.phpbb-menu');

	// Responsive navbar menu
	$phpbbNavbar.find('#phpbb-menu-toggle').on('click', function (e) {
		$phpbbMenu.toggleClass('show');
		$phpbbNavbar.toggleClass('menu-open');

		e.preventDefault();
	});

	// Hide active dropdowns/menus when click event happens outside
	$('body').click(function(e) {
		var $parents = $(e.target).parents();
		if (!$parents.is($phpbbNavbar)) {
			$phpbbMenu.removeClass('show');
			$phpbbNavbar.removeClass('menu-open');
		}
		if (typeof $phpbbSidebar !== 'undefined' && !$parents.is($phpbbSidebar)) {
			$phpbbSidebar.removeClass('show');
		}
	});

});
