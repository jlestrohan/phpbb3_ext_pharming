<?php
/**
*
* @package Header Link
* @copyright (c) 2015 HiFiKabin
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace hifikabin\headerlink\acp;

class headerlink_module
{
var $u_action;

	function main($id, $mode)
	{
		global $user, $template, $request;
		global $config;

		$this->tpl_name = 'acp_headerlink_config';
		$this->page_title = $user->lang('HEADERLINK_CONFIG');
		$form_name = 'acp_headerlink_config';
		add_form_key($form_name);
		
		$this->is_phpbb32 = phpbb_version_compare($config['version'], '3.2.0@dev', '>=') && phpbb_version_compare($config['version'], '3.3.0@dev', '<');	

	$submit = $request->is_set_post('submit');
	if ($submit)
		{
		if (!check_form_key('acp_headerlink_config'))
		{
			trigger_error('FORM_INVALID');
		}
		$config->set('headerlink_enable', $request->variable('headerlink_enable', ''));
		$config->set('headerlink_responsive', $request->variable('headerlink_responsive', ''));
		$config->set('headerlink_navbar', $request->variable('headerlink_navbar', ''));

		$config->set('headerlink_url_1', $request->variable('headerlink_url_1', '', true));
		$config->set('headerlink_name_1', $request->variable('headerlink_name_1', '', true));
		$config->set('headerlink_hover_1', $request->variable('headerlink_hover_1', '', true));
		$config->set('headerlink_active_1', $request->variable('headerlink_active_1', 0));
		$config->set('headerlink_target_1', $request->variable('headerlink_target_1', 0));

		$config->set('headerlink_url_2', $request->variable('headerlink_url_2', '', true));
		$config->set('headerlink_name_2', $request->variable('headerlink_name_2', '', true));
		$config->set('headerlink_hover_2', $request->variable('headerlink_hover_2', '', true));
		$config->set('headerlink_active_2', $request->variable('headerlink_active_2', 0));
		$config->set('headerlink_target_2', $request->variable('headerlink_target_2', 0));


		$config->set('headerlink_url_3', $request->variable('headerlink_url_3', '', true));
		$config->set('headerlink_name_3', $request->variable('headerlink_name_3', '', true));
		$config->set('headerlink_hover_3', $request->variable('headerlink_hover_3', '', true));
		$config->set('headerlink_active_3', $request->variable('headerlink_active_3', 0));
		$config->set('headerlink_target_3', $request->variable('headerlink_target_3', 0));

		$config->set('headerlink_url_4', $request->variable('headerlink_url_4', '', true));
		$config->set('headerlink_name_4', $request->variable('headerlink_name_4', '', true));
		$config->set('headerlink_hover_4', $request->variable('headerlink_hover_4', '', true));
		$config->set('headerlink_active_4', $request->variable('headerlink_active_4', 0));
		$config->set('headerlink_target_4', $request->variable('headerlink_target_4', 0));


		$config->set('headerlink_url_5', $request->variable('headerlink_url_5', '', true));
		$config->set('headerlink_name_5', $request->variable('headerlink_name_5', '', true));
		$config->set('headerlink_hover_5', $request->variable('headerlink_hover_5', '', true));
		$config->set('headerlink_active_5', $request->variable('headerlink_active_5', 0));
		$config->set('headerlink_target_5', $request->variable('headerlink_target_5', 0));


		$config->set('headerlink_url_6', $request->variable('headerlink_url_6', '', true));
		$config->set('headerlink_name_6', $request->variable('headerlink_name_6', '', true));
		$config->set('headerlink_hover_6', $request->variable('headerlink_hover_6', '', true));
		$config->set('headerlink_active_6', $request->variable('headerlink_active_6', 0));
		$config->set('headerlink_target_6', $request->variable('headerlink_target_6', 0));


		$config->set('headerlink_url_7', $request->variable('headerlink_url_7', '', true));
		$config->set('headerlink_name_7', $request->variable('headerlink_name_7', '', true));
		$config->set('headerlink_hover_7', $request->variable('headerlink_hover_7', '', true));
		$config->set('headerlink_active_7', $request->variable('headerlink_active_7', 0));
		$config->set('headerlink_target_7', $request->variable('headerlink_target_7', 0));


		$config->set('headerlink_url_8', $request->variable('headerlink_url_8', '', true));
		$config->set('headerlink_name_8', $request->variable('headerlink_name_8', '', true));
		$config->set('headerlink_hover_8', $request->variable('headerlink_hover_8', '', true));
		$config->set('headerlink_active_8', $request->variable('headerlink_active_8', 0));
		$config->set('headerlink_target_8', $request->variable('headerlink_target_8', 0));

		$config->set('headerlink_colour_1', $request->variable('headerlink_colour_1', ''));
		$config->set('headerlink_colour_2', $request->variable('headerlink_colour_2', ''));
		$config->set('headerlink_colour_3', $request->variable('headerlink_colour_3', ''));
		$config->set('headerlink_colour_4', $request->variable('headerlink_colour_4', ''));
		$config->set('headerlink_colour_5', $request->variable('headerlink_colour_5', ''));
		$config->set('headerlink_colour_6', $request->variable('headerlink_colour_6', ''));
		$config->set('headerlink_colour_7', $request->variable('headerlink_colour_7', ''));
		$config->set('headerlink_colour_8', $request->variable('headerlink_colour_8', ''));

		$config->set('headerlink_icon_1', $request->variable('headerlink_icon_1', ''));
		$config->set('headerlink_icon_2', $request->variable('headerlink_icon_2', ''));
		$config->set('headerlink_icon_3', $request->variable('headerlink_icon_3', ''));
		$config->set('headerlink_icon_4', $request->variable('headerlink_icon_4', ''));
		$config->set('headerlink_icon_5', $request->variable('headerlink_icon_5', ''));
		$config->set('headerlink_icon_6', $request->variable('headerlink_icon_6', ''));
		$config->set('headerlink_icon_7', $request->variable('headerlink_icon_7', ''));
		$config->set('headerlink_icon_8', $request->variable('headerlink_icon_8', ''));


		trigger_error($user->lang('HEADERLINK_SAVED') . adm_back_link($this->u_action));

		}
		$template->assign_vars(array(

			'HEADERLINK_ENABLE'				=> (isset($config['headerlink_enable'])) ? $config['headerlink_enable'] : '',
			'HEADERLINK_RESPONSIVE'			=> (isset($config['headerlink_responsive'])) ? $config['headerlink_responsive'] : '',
			'HEADERLINK_NAVBAR'				=> (isset($config['headerlink_navbar'])) ? $config['headerlink_navbar'] : '',

			'HEADERLINK_URL_1'				=> (isset($config['headerlink_url_1'])) ? $config['headerlink_url_1'] : '',
			'HEADERLINK_NAME_1'				=> (isset($config['headerlink_name_1'])) ? $config['headerlink_name_1'] : '',
			'HEADERLINK_HOVER_1'			=> (isset($config['headerlink_hover_1'])) ? $config['headerlink_hover_1'] : '',
			'HEADERLINK_ACTIVE_1'			=> (isset($config['headerlink_active_1'])) ? $config['headerlink_active_1'] : '',
			'HEADERLINK_TARGET_1'			=> (!empty($config['headerlink_target_1'])) ? true : false,

			'HEADERLINK_URL_2'				=> (isset($config['headerlink_url_2'])) ? $config['headerlink_url_2'] : '',
			'HEADERLINK_NAME_2'				=> (isset($config['headerlink_name_2'])) ? $config['headerlink_name_2'] : '',
			'HEADERLINK_HOVER_2'			=> (isset($config['headerlink_hover_2'])) ? $config['headerlink_hover_2'] : '',
			'HEADERLINK_ACTIVE_2'			=> (isset($config['headerlink_active_2'])) ? $config['headerlink_active_2'] : '',
			'HEADERLINK_TARGET_2'			=> (!empty($config['headerlink_target_2'])) ? true : false,

			'HEADERLINK_URL_3'				=> (isset($config['headerlink_url_3'])) ? $config['headerlink_url_3'] : '',
			'HEADERLINK_NAME_3'				=> (isset($config['headerlink_name_3'])) ? $config['headerlink_name_3'] : '',
			'HEADERLINK_HOVER_3'			=> (isset($config['headerlink_hover_3'])) ? $config['headerlink_hover_3'] : '',
			'HEADERLINK_ACTIVE_3'			=> (isset($config['headerlink_active_3'])) ? $config['headerlink_active_3'] : '',
			'HEADERLINK_TARGET_3'			=> (!empty($config['headerlink_target_3'])) ? true : false,

			'HEADERLINK_URL_4'				=> (isset($config['headerlink_url_4'])) ? $config['headerlink_url_4'] : '',
			'HEADERLINK_NAME_4'				=> (isset($config['headerlink_name_4'])) ? $config['headerlink_name_4'] : '',
			'HEADERLINK_HOVER_4'			=> (isset($config['headerlink_hover_4'])) ? $config['headerlink_hover_4'] : '',
			'HEADERLINK_ACTIVE_4'			=> (isset($config['headerlink_active_4'])) ? $config['headerlink_active_4'] : '',
			'HEADERLINK_TARGET_4'			=> (!empty($config['headerlink_target_4'])) ? true : false,


			'HEADERLINK_URL_5'				=> (isset($config['headerlink_url_5'])) ? $config['headerlink_url_5'] : '',
			'HEADERLINK_NAME_5'				=> (isset($config['headerlink_name_5'])) ? $config['headerlink_name_5'] : '',
			'HEADERLINK_HOVER_5'			=> (isset($config['headerlink_hover_5'])) ? $config['headerlink_hover_5'] : '',
			'HEADERLINK_ACTIVE_5'			=> (isset($config['headerlink_active_5'])) ? $config['headerlink_active_5'] : '',
			'HEADERLINK_TARGET_5'			=> (!empty($config['headerlink_target_5'])) ? true : false,


			'HEADERLINK_URL_6'				=> (isset($config['headerlink_url_6'])) ? $config['headerlink_url_6'] : '',
			'HEADERLINK_NAME_6'				=> (isset($config['headerlink_name_6'])) ? $config['headerlink_name_6'] : '',
			'HEADERLINK_HOVER_6'			=> (isset($config['headerlink_hover_6'])) ? $config['headerlink_hover_6'] : '',
			'HEADERLINK_ACTIVE_6'			=> (isset($config['headerlink_active_6'])) ? $config['headerlink_active_6'] : '',
			'HEADERLINK_TARGET_6'			=> (!empty($config['headerlink_target_6'])) ? true : false,


			'HEADERLINK_URL_7'				=> (isset($config['headerlink_url_7'])) ? $config['headerlink_url_7'] : '',
			'HEADERLINK_NAME_7'				=> (isset($config['headerlink_name_7'])) ? $config['headerlink_name_7'] : '',
			'HEADERLINK_HOVER_7'			=> (isset($config['headerlink_hover_7'])) ? $config['headerlink_hover_7'] : '',
			'HEADERLINK_ACTIVE_7'			=> (isset($config['headerlink_active_7'])) ? $config['headerlink_active_7'] : '',
			'HEADERLINK_TARGET_7'			=> (!empty($config['headerlink_target_7'])) ? true : false,


			'HEADERLINK_URL_8'				=> (isset($config['headerlink_url_8'])) ? $config['headerlink_url_8'] : '',
			'HEADERLINK_NAME_8'				=> (isset($config['headerlink_name_8'])) ? $config['headerlink_name_8'] : '',
			'HEADERLINK_HOVER_8'			=> (isset($config['headerlink_hover_8'])) ? $config['headerlink_hover_8'] : '',
			'HEADERLINK_ACTIVE_8'			=> (isset($config['headerlink_active_8'])) ? $config['headerlink_active_8'] : '',
			'HEADERLINK_TARGET_8'			=> (!empty($config['headerlink_target_8'])) ? true : false,

			'HEADERLINK_COLOUR_1'			=> (isset($config['headerlink_colour_1'])) ? $config['headerlink_colour_1'] : '',
			'HEADERLINK_COLOUR_2'			=> (isset($config['headerlink_colour_2'])) ? $config['headerlink_colour_2'] : '',
			'HEADERLINK_COLOUR_3'			=> (isset($config['headerlink_colour_3'])) ? $config['headerlink_colour_3'] : '',
			'HEADERLINK_COLOUR_4'			=> (isset($config['headerlink_colour_4'])) ? $config['headerlink_colour_4'] : '',
			'HEADERLINK_COLOUR_5'			=> (isset($config['headerlink_colour_5'])) ? $config['headerlink_colour_5'] : '',
			'HEADERLINK_COLOUR_6'			=> (isset($config['headerlink_colour_6'])) ? $config['headerlink_colour_6'] : '',
			'HEADERLINK_COLOUR_7'			=> (isset($config['headerlink_colour_7'])) ? $config['headerlink_colour_7'] : '',
			'HEADERLINK_COLOUR_8'			=> (isset($config['headerlink_colour_8'])) ? $config['headerlink_colour_8'] : '',

			'HEADERLINK_ICON_1'				=> (isset($config['headerlink_icon_1'])) ? $config['headerlink_icon_1'] : '',
			'HEADERLINK_ICON_2'				=> (isset($config['headerlink_icon_2'])) ? $config['headerlink_icon_2'] : '',
			'HEADERLINK_ICON_3'				=> (isset($config['headerlink_icon_3'])) ? $config['headerlink_icon_3'] : '',
			'HEADERLINK_ICON_4'				=> (isset($config['headerlink_icon_4'])) ? $config['headerlink_icon_4'] : '',
			'HEADERLINK_ICON_5'				=> (isset($config['headerlink_icon_5'])) ? $config['headerlink_icon_5'] : '',
			'HEADERLINK_ICON_6'				=> (isset($config['headerlink_icon_6'])) ? $config['headerlink_icon_6'] : '',
			'HEADERLINK_ICON_7'				=> (isset($config['headerlink_icon_7'])) ? $config['headerlink_icon_7'] : '',
			'HEADERLINK_ICON_8'				=> (isset($config['headerlink_icon_8'])) ? $config['headerlink_icon_8'] : '',

			'U_ACTION'						=> $this->u_action,
		));
	}
}
