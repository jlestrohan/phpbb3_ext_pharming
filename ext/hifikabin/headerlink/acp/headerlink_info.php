<?php
/**
*
* @package Header Link
* @copyright (c) 2015 HiFiKabin
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace hifikabin\headerlink\acp;

class pmwelcome_info
{
	function module()
	{
		return array(
			'filename'	=> '\hifikabin\headerlink\acp\headerlink_module',
			'title'		=> 'ACP_HEADERLINK',
			'modes'		=> array(
			'settings'	=> array('title' => 'ACP_HEADERLINK_SETTINGS', 'auth' => 'ext_hifikabin\headerlink && acl_a_board', 'cat' => array('ACP_HEADERLINK')),
			),
		);
	}
}
