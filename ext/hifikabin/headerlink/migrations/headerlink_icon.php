<?php
/**
*
* @package phpBB Extension - Header Link
* @copyright (c) 2015 HiFiKabin
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace hifikabin\headerlink\migrations;

class headerlink_icon extends \phpbb\db\migration\migration
{
	static public function depends_on()
	{
		return array('\hifikabin\headerlink\migrations\headerlink_colour');
	}

	public function update_data()
	{
		return array(
			// Add configs
			array('config.add', array('headerlink_icon_1', '')),
			array('config.add', array('headerlink_icon_2', '')),
			array('config.add', array('headerlink_icon_3', '')),
			array('config.add', array('headerlink_icon_4', '')),
			array('config.add', array('headerlink_icon_5', '')),
			array('config.add', array('headerlink_icon_6', '')),
			array('config.add', array('headerlink_icon_7', '')),
			array('config.add', array('headerlink_icon_8', '')),
		);
	}
}
