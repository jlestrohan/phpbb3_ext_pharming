<?php
/**
*
* @package phpBB Extension - Header Link
* @copyright (c) 2015 HiFiKabin
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace hifikabin\headerlink\migrations;

class headerlink_additions extends \phpbb\db\migration\migration
{
	static public function depends_on()
	{
		return array('\hifikabin\headerlink\migrations\headerlink_icon');
	}

	public function update_data()
	{
		return array(
			// Add configs
			array('config.add', array('headerlink_enable', 0)),
			array('config.add', array('headerlink_responsive', 0)),
			array('config.add', array('headerlink_navbar', 1)),
		);
	}
}
