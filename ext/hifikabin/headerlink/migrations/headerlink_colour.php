<?php
/**
*
* @package phpBB Extension - Header Link
* @copyright (c) 2015 HiFiKabin
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace hifikabin\headerlink\migrations;

class headerlink_colour extends \phpbb\db\migration\migration
{
	static public function depends_on()
	{
		return array('\hifikabin\headerlink\migrations\headerlink_data');
	}

	public function update_data()
	{
		return array(
			// Add configs
			array('config.add', array('headerlink_colour_1', '#0C93D7')),
			array('config.add', array('headerlink_colour_2', '#0C93D7')),
			array('config.add', array('headerlink_colour_3', '#0C93D7')),
			array('config.add', array('headerlink_colour_4', '#0C93D7')),
			array('config.add', array('headerlink_colour_5', '#0C93D7')),
			array('config.add', array('headerlink_colour_6', '#0C93D7')),
			array('config.add', array('headerlink_colour_7', '#0C93D7')),
			array('config.add', array('headerlink_colour_8', '#0C93D7')),
		);
	}
}
