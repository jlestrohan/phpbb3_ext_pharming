<?php
/**
*
* @package phpBB Extension - Headwer Link
* @copyright (c) 2015 HiFiKabin
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace hifikabin\headerlink\migrations;

class headerlink_data extends \phpbb\db\migration\migration
{
	public function update_data()
	{
		return array(
			// Add configs
			array('config.add', array('headerlink_url_1', '')),
			array('config.add', array('headerlink_name_1', '')),
			array('config.add', array('headerlink_hover_1', '')),
			array('config.add', array('headerlink_target_1', '')),
			array('config.add', array('headerlink_active_1', '')),

			array('config.add', array('headerlink_url_2', '')),
			array('config.add', array('headerlink_name_2', '')),
			array('config.add', array('headerlink_hover_2', '')),
			array('config.add', array('headerlink_target_2', '')),
			array('config.add', array('headerlink_active_2', '')),

			array('config.add', array('headerlink_url_3', '')),
			array('config.add', array('headerlink_name_3', '')),
			array('config.add', array('headerlink_hover_3', '')),
			array('config.add', array('headerlink_target_3', '')),
			array('config.add', array('headerlink_active_3', '')),

			array('config.add', array('headerlink_url_4', '')),
			array('config.add', array('headerlink_name_4', '')),
			array('config.add', array('headerlink_hover_4', '')),
			array('config.add', array('headerlink_target_4', '')),
			array('config.add', array('headerlink_active_4', '')),

			array('config.add', array('headerlink_url_5', '')),
			array('config.add', array('headerlink_name_5', '')),
			array('config.add', array('headerlink_hover_5', '')),
			array('config.add', array('headerlink_target_5', '')),
			array('config.add', array('headerlink_active_5', '')),

			array('config.add', array('headerlink_url_6', '')),
			array('config.add', array('headerlink_name_6', '')),
			array('config.add', array('headerlink_hover_6', '')),
			array('config.add', array('headerlink_target_6', '')),
			array('config.add', array('headerlink_active_6', '')),

			array('config.add', array('headerlink_url_7', '')),
			array('config.add', array('headerlink_name_7', '')),
			array('config.add', array('headerlink_hover_7', '')),
			array('config.add', array('headerlink_target_7', '')),
			array('config.add', array('headerlink_active_7', '')),

			array('config.add', array('headerlink_url_8', '')),
			array('config.add', array('headerlink_name_8', '')),
			array('config.add', array('headerlink_hover_8', '')),
			array('config.add', array('headerlink_target_8', '')),
			array('config.add', array('headerlink_active_8', '')),

			// Add ACP modules
			array('module.add', array('acp', 'ACP_BOARD_CONFIGURATION', array(
				'module_basename'	=> '\hifikabin\headerlink\acp\headerlink_module',
				'module_langname'	=> 'ACP_HEADERLINK',
				'module_mode'		=> 'settings',
				'module_auth'		=> 'ext_hifikabin/headerlink && acl_a_board',
			))),
		);
	}
}
