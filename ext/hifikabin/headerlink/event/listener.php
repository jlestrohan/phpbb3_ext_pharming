<?php
/**
*
* @package phpBB Extension - Header Link
* @copyright (c) 2015 HiFiKabin
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

namespace hifikabin\headerlink\event;

/**
* @ignore
*/
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class listener implements EventSubscriberInterface
{
	static public function getSubscribedEvents()
	{
		return array(
			'core.user_setup'		=> 'load_language_on_setup',
			'core.page_header'		=> 'add_page_header_link',
		);
	}

	protected $user;

	protected $template;

	protected $config;	

	public function __construct(\phpbb\user $user, \phpbb\template\template $template, \phpbb\config\config $config)
		{
		$this->user = $user;
		$this->template = $template;
		$this->config = $config;
		}

	public function load_language_on_setup($event)
	{
		$lang_set_ext = $event['lang_set_ext'];
		$lang_set_ext[] = array(
		'ext_name' => 'hifikabin/headerlink',
		'lang_set' => 'common',
		);
		$event['lang_set_ext'] = $lang_set_ext;
	}

	public function add_page_header_link($event)
	{
		$this->user->add_lang_ext('hifikabin/headerlink', 'common');

		$this->template->assign_vars(array(

		'HEADERLINK_ENABLE'				=> $this->config['headerlink_enable'],
		'HEADERLINK_RESPONSIVE'			=> $this->config['headerlink_responsive'] ,
		'HEADERLINK_NAVBAR'				=> $this->config['headerlink_navbar'] ,

		'HEADERLINK_URL_1'				=> (isset($this->config['headerlink_url_1'])) ? $this->config['headerlink_url_1'] : '',
		'HEADERLINK_NAME_1'				=> (isset($this->config['headerlink_name_1'])) ? $this->config['headerlink_name_1'] : '',
		'HEADERLINK_HOVER_1'			=> (isset($this->config['headerlink_hover_1'])) ? $this->config['headerlink_hover_1'] : '',
		'HEADERLINK_ACTIVE_1'			=> (isset($this->config['headerlink_active_1'])) ? $this->config['headerlink_active_1'] : '',
		'HEADERLINK_TARGET_1'			=> $this->config['headerlink_target_1'] ? '_blank' : '_self',

		'HEADERLINK_URL_2'				=> (isset($this->config['headerlink_url_2'])) ? $this->config['headerlink_url_2'] : '',
		'HEADERLINK_NAME_2'				=> (isset($this->config['headerlink_name_2'])) ? $this->config['headerlink_name_2'] : '',
		'HEADERLINK_HOVER_2'			=> (isset($this->config['headerlink_hover_2'])) ? $this->config['headerlink_hover_2'] : '',
		'HEADERLINK_ACTIVE_2'			=> (isset($this->config['headerlink_active_2'])) ? $this->config['headerlink_active_2'] : '',
		'HEADERLINK_TARGET_2'			=> $this->config['headerlink_target_2'] ? '_blank' : '_self',

		'HEADERLINK_URL_3'				=> (isset($this->config['headerlink_url_3'])) ? $this->config['headerlink_url_3'] : '',
		'HEADERLINK_NAME_3'				=> (isset($this->config['headerlink_name_3'])) ? $this->config['headerlink_name_3'] : '',
		'HEADERLINK_HOVER_3'			=> (isset($this->config['headerlink_hover_3'])) ? $this->config['headerlink_hover_3'] : '',
		'HEADERLINK_ACTIVE_3'			=> (isset($this->config['headerlink_active_3'])) ? $this->config['headerlink_active_3'] : '',
		'HEADERLINK_TARGET_3'			=> $this->config['headerlink_target_3'] ? '_blank' : '_self',

		'HEADERLINK_URL_4'				=> (isset($this->config['headerlink_url_4'])) ? $this->config['headerlink_url_4'] : '',
		'HEADERLINK_NAME_4'				=> (isset($this->config['headerlink_name_4'])) ? $this->config['headerlink_name_4'] : '',
		'HEADERLINK_HOVER_4'			=> (isset($this->config['headerlink_hover_4'])) ? $this->config['headerlink_hover_4'] : '',
		'HEADERLINK_ACTIVE_4'			=> (isset($this->config['headerlink_active_4'])) ? $this->config['headerlink_active_4'] : '',
		'HEADERLINK_TARGET_4'			=> $this->config['headerlink_target_4'] ? '_blank' : '_self',

		'HEADERLINK_URL_5'				=> (isset($this->config['headerlink_url_5'])) ? $this->config['headerlink_url_5'] : '',
		'HEADERLINK_NAME_5'				=> (isset($this->config['headerlink_name_5'])) ? $this->config['headerlink_name_5'] : '',
		'HEADERLINK_HOVER_5'			=> (isset($this->config['headerlink_hover_5'])) ? $this->config['headerlink_hover_5'] : '',
		'HEADERLINK_ACTIVE_5'			=> (isset($this->config['headerlink_active_5'])) ? $this->config['headerlink_active_5'] : '',
		'HEADERLINK_TARGET_5'			=> $this->config['headerlink_target_5'] ? '_blank' : '_self',

		'HEADERLINK_URL_6'				=> (isset($this->config['headerlink_url_6'])) ? $this->config['headerlink_url_6'] : '',
		'HEADERLINK_NAME_6'				=> (isset($this->config['headerlink_name_6'])) ? $this->config['headerlink_name_6'] : '',
		'HEADERLINK_HOVER_6'			=> (isset($this->config['headerlink_hover_6'])) ? $this->config['headerlink_hover_6'] : '',
		'HEADERLINK_ACTIVE_6'			=> (isset($this->config['headerlink_active_6'])) ? $this->config['headerlink_active_6'] : '',
		'HEADERLINK_TARGET_6'			=> $this->config['headerlink_target_6'] ? '_blank' : '_self',

		'HEADERLINK_URL_7'				=> (isset($this->config['headerlink_url_7'])) ? $this->config['headerlink_url_7'] : '',
		'HEADERLINK_NAME_7'				=> (isset($this->config['headerlink_name_7'])) ? $this->config['headerlink_name_7'] : '',
		'HEADERLINK_HOVER_7'			=> (isset($this->config['headerlink_hover_7'])) ? $this->config['headerlink_hover_7'] : '',
		'HEADERLINK_ACTIVE_7'			=> (isset($this->config['headerlink_active_7'])) ? $this->config['headerlink_active_7'] : '',
		'HEADERLINK_TARGET_7'			=> $this->config['headerlink_target_7'] ? '_blank' : '_self',

		'HEADERLINK_URL_8'				=> (isset($this->config['headerlink_url_8'])) ? $this->config['headerlink_url_8'] : '',
		'HEADERLINK_NAME_8'				=> (isset($this->config['headerlink_name_8'])) ? $this->config['headerlink_name_8'] : '',
		'HEADERLINK_HOVER_8'			=> (isset($this->config['headerlink_hover_8'])) ? $this->config['headerlink_hover_8'] : '',
		'HEADERLINK_ACTIVE_8'			=> (isset($this->config['headerlink_active_8'])) ? $this->config['headerlink_active_8'] : '',
		'HEADERLINK_TARGET_8'			=> $this->config['headerlink_target_8'] ? '_blank' : '_self',

		'HEADERLINK_COLOUR_1'			=> (isset($this->config['headerlink_colour_1'])) ? $this->config['headerlink_colour_1'] : '',
		'HEADERLINK_COLOUR_2'			=> (isset($this->config['headerlink_colour_2'])) ? $this->config['headerlink_colour_2'] : '',
		'HEADERLINK_COLOUR_3'			=> (isset($this->config['headerlink_colour_3'])) ? $this->config['headerlink_colour_3'] : '',
		'HEADERLINK_COLOUR_4'			=> (isset($this->config['headerlink_colour_4'])) ? $this->config['headerlink_colour_4'] : '',
		'HEADERLINK_COLOUR_5'			=> (isset($this->config['headerlink_colour_5'])) ? $this->config['headerlink_colour_5'] : '',
		'HEADERLINK_COLOUR_6'			=> (isset($this->config['headerlink_colour_6'])) ? $this->config['headerlink_colour_6'] : '',
		'HEADERLINK_COLOUR_7'			=> (isset($this->config['headerlink_colour_7'])) ? $this->config['headerlink_colour_7'] : '',
		'HEADERLINK_COLOUR_8'			=> (isset($this->config['headerlink_colour_8'])) ? $this->config['headerlink_colour_8'] : '',

		'HEADERLINK_ICON_1'				=> (isset($this->config['headerlink_icon_1'])) ? $this->config['headerlink_icon_1'] : '',
		'HEADERLINK_ICON_2'				=> (isset($this->config['headerlink_icon_2'])) ? $this->config['headerlink_icon_2'] : '',
		'HEADERLINK_ICON_3'				=> (isset($this->config['headerlink_icon_3'])) ? $this->config['headerlink_icon_3'] : '',
		'HEADERLINK_ICON_4'				=> (isset($this->config['headerlink_icon_4'])) ? $this->config['headerlink_icon_4'] : '',
		'HEADERLINK_ICON_5'				=> (isset($this->config['headerlink_icon_5'])) ? $this->config['headerlink_icon_5'] : '',
		'HEADERLINK_ICON_6'				=> (isset($this->config['headerlink_icon_6'])) ? $this->config['headerlink_icon_6'] : '',
		'HEADERLINK_ICON_7'				=> (isset($this->config['headerlink_icon_7'])) ? $this->config['headerlink_icon_7'] : '',
		'HEADERLINK_ICON_8'				=> (isset($this->config['headerlink_icon_8'])) ? $this->config['headerlink_icon_8'] : '',
		));
	}
}
