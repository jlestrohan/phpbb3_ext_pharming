<?php
/**
*
* @package phpBB Extension - Header Links
* @copyright (c) 2015 HiFiKabin
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/


/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
   exit;
}

if (empty($lang) || !is_array($lang))
{
   $lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(

    'ACP_HEADERLINK'                            => 'Header Link',
    'ACP_HEADERLINK_CONFIG'                     => 'Header Link',
    'ACP_HEADERLINK_CONFIG_EXPLAIN'             => 'Das ist die Konfigurationsseite für die Header Link Extension.' ,
    'ACP_HEADERLINK_INFO'                       => 'Hier kannst du bis zu 8 Links einfügen, jeden mit eigener Benennung und eigenem Ziel. Fülle nur das Formular unten aus',
    'ACP_HEADERLINK_INT_URL'                    => 'Interne URLs können ohne http:// und Domain eingetragen werden Z.B. app.php/help/faq ',
    'ACP_HEADERLINK_EXT_URL'                    => 'Externe URLs müssen mit http:// eingetragen werden ',

    'ACP_HEADERLINK_LINK_1'                     => 'Erster Link',
    'ACP_HEADERLINK_LINK_2'                     => 'Zweiter Link',
    'ACP_HEADERLINK_LINK_3'                     => 'Dritter Link',
    'ACP_HEADERLINK_LINK_4'                     => 'Vierter Link',
    'ACP_HEADERLINK_LINK_5'                     => 'Fünfter Link',
    'ACP_HEADERLINK_LINK_6'                     => 'Sechster Link',
    'ACP_HEADERLINK_LINK_7'                     => 'Siebter Link',
    'ACP_HEADERLINK_LINK_8'                     => 'Achter Link',

    'ACP_HEADERLINK_URL_PLACEHOLDER'            => 'Link URL',
    'ACP_HEADERLINK_NAME_PLACEHOLDER'           => 'Link Name',
    'ACP_HEADERLINK_HOVER_PLACEHOLDER'          => 'Link bei Mouse over',
    'ACP_HEADERLINK_TARGET'                     => 'Öffne Link in neuem Tab',
    'ACP_HEADERLINK_DISABLE'                    => 'Schalte diesen Link aus',
    'ACP_HEADERLINK_GUEST'                      => 'Aktiviere diesen Link nur für Gäste',
    'ACP_HEADERLINK_MEMBER'                     => 'Aktiviere diesen Link nur für Mitglieder',
    'ACP_HEADERLINK_ACTIVE'                     => 'Aktiviere diesen Link für alle',

    'HEADERLINK_CONFIG'                         => 'Header Link Einstellungen',
    'HEADERLINK_SAVED'                          => 'Header Link Einstellungen gespeichert',

));
