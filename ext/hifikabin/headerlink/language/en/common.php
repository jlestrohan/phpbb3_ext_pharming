<?php
/**
*
* @package phpBB Extension - Header Links
* @copyright (c) 2015 HiFiKabin
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/


/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(

	'ACP_HEADERLINK'							=> 'Header Link',
	'ACP_HEADERLINK_CONFIG'                     => 'Header Link',
	'ACP_HEADERLINK_CONFIG_EXPLAIN'				=> 'This is the Configuration page for the Header Link Extension.' ,
	'ACP_HEADERLINK_INFO'                       => 'Here you can enter upto 8 links each with its own name and target. Just enter the details below',
	'ACP_HEADERLINK_INT_URL'                    => 'Internal URL\'s can be entered without http:// and the domain name eg app.php/help/faq ',
	'ACP_HEADERLINK_EXT_URL'                    => 'External URL\'s must be entered in full including http:// ',
	'ACP_HEADERLINK_ICON'                       => 'FA Icon codes can be found in the following link. Preceed the code with ’icon’ eg icon fa-credit-card',

	'HEADERLINK_ENABLE'							=> 'Enable Header Links.',
	'HEADERLINK_ENABLE_EXPLAIN'					=> 'Do you want to enable the Header Links ext?',
	'HEADERLINK_RESPONSIVE'						=> 'Responsive.',
	'HEADERLINK_RESPONSIVE_EXPLAIN'				=> 'Do you want all of the links to be shown on small screens?',
	'HEADERLINK_NAVBAR'							=> 'Link Bar Position',
	'HEADERLINK_NAVBAR_EXPLAIN'					=> 'Where do you want to display the Links Bar?',
	
	'HEADERLINK_SWITCH_HEADER'					=> 'Header.',
	'HEADERLINK_SWITCH_NAVBAR'					=> 'Navbar.',

	'ACP_HEADERLINK_LINK_1'						=> 'First Link',
	'ACP_HEADERLINK_LINK_2'						=> 'Second Link',
	'ACP_HEADERLINK_LINK_3'						=> 'Third Link',
	'ACP_HEADERLINK_LINK_4'						=> 'Fourth Link',
	'ACP_HEADERLINK_LINK_5'						=> 'Fifth Link',
	'ACP_HEADERLINK_LINK_6'						=> 'Sixth Link',
	'ACP_HEADERLINK_LINK_7'						=> 'Seventh Link',
	'ACP_HEADERLINK_LINK_8'						=> 'Eight Link',

	'ACP_HEADERLINK_URL_PLACEHOLDER'			=> 'Link URL',
	'ACP_HEADERLINK_NAME_PLACEHOLDER'			=> 'Link Name',
	'ACP_HEADERLINK_HOVER_PLACEHOLDER'			=> 'Link on hover',
	'ACP_HEADERLINK_TARGET'						=> 'Open in new tab?',
	'ACP_HEADERLINK_DISABLE'					=> 'Disable this Link?',
	'ACP_HEADERLINK_GUEST'						=> 'Activate this Link for Guests Only?',
	'ACP_HEADERLINK_MEMBER'						=> 'Activate this Link for Members Only?',
	'ACP_HEADERLINK_ACTIVE'						=> 'Activate this Link for Everyone?',
	'ACP_HEADERLINK_ADMIN'						=> 'Activate this Link for Administrators Only?',
	'ACP_HEADERLINK_MOD'						=> 'Activate this Link for Moderators Only?',

	'ACP_HEADERLINK_COLOUR'						=> 'Click box to select Button Colour.',
	'ACP_HEADERLINK_ICON_PLACEHOLDER'			=> 'FA Icon code (see note above)',

	'HEADERLINK_CONFIG'							=> 'Header Link Settings',
	'HEADERLINK_SAVED'							=> 'Header Link settings saved',

	'HEADERLINK_MENU'							=> 'Links',
	'HEADERLINK_MENU_HOVER'						=> 'Show Links',
));
