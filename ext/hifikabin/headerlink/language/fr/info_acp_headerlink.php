<?php
/**
*
* Header Link extension for the phpBB Forum Software package.
* French translation by Galixte (http://www.galixte.com)
*
* @copyright (c) 2017 HiFiKabin <https://phpbb.hifikabin.me.uk>
* @license GNU General Public License, version 2 (GPL-2.0)
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ « » “ ” …
//

$lang = array_merge($lang, array(

	'ACP_HEADERLINK'							=> 'Liens de l’entête',
	'ACP_HEADERLINK_CONFIG'						=> 'Liens de l’entête',
	'ACP_HEADERLINK_CONFIG_EXPLAIN'				=> 'Sur cette page il est possible de configurer l’extension « Liens de l’entête ».' ,
	'ACP_HEADERLINK_INFO'						=> 'Jusqu’à 8 liens peuvent être configurés comprenant chacun un nom et une adresse URL tout deux personnalisables. Pour cela, saisir les informations ci-dessous.',
	'ACP_HEADERLINK_INT_URL'					=> 'Les adresses URL interne au forum peuvent être saisies sans : « http:// » ni sans nom de domaine, tel que par exemple : app.php/help/faq.',
	'ACP_HEADERLINK_EXT_URL'					=> 'Les adresses URL externes au forum doivent comporter toutes les informations « http:// » compris.',
	'ACP_HEADERLINK_ICON'						=> 'Les codes des icônes FA sont disponibles sur le lien suivant : http://fontawesome.io. Il est nécessaire de précéder le nom de l’icône par : « icon », tel que par exemple : « icon fa-credit-card ».',

	'ACP_HEADERLINK_LINK_1'						=> 'Premier lien',
	'ACP_HEADERLINK_LINK_2'						=> 'Second lien',
	'ACP_HEADERLINK_LINK_3'						=> 'Troisième lien',
	'ACP_HEADERLINK_LINK_4'						=> 'Quatrième lien',
	'ACP_HEADERLINK_LINK_5'						=> 'Cinquième lien',
	'ACP_HEADERLINK_LINK_6'						=> 'Sixième lien',
	'ACP_HEADERLINK_LINK_7'						=> 'Septième lien',
	'ACP_HEADERLINK_LINK_8'						=> 'Huitième lien',

	'ACP_HEADERLINK_URL_PLACEHOLDER'			=> 'Adresse URL du lien',
	'ACP_HEADERLINK_NAME_PLACEHOLDER'			=> 'Nom du lien affiché',
	'ACP_HEADERLINK_HOVER_PLACEHOLDER'			=> 'Nom du lien affiché au survol de la souris sur le bouton',
	'ACP_HEADERLINK_TARGET'						=> 'Ouvrir le lien dans un nouvel onglet',
	'ACP_HEADERLINK_DISABLE'					=> 'Désactiver ce lien',
	'ACP_HEADERLINK_GUEST' 						=> 'Activer ce lien uniquement pour les invités',
	'ACP_HEADERLINK_MEMBER'						=> 'Activer ce lien uniquement pour les membres',
	'ACP_HEADERLINK_ACTIVE'						=> 'Activer ce lien pour tout le monde',
	'ACP_HEADERLINK_ADMIN'						=> 'Activer ce lien uniquement pour les administrateurs',
	'ACP_HEADERLINK_MOD'						=> 'Activer ce lien uniquement pour les modérateurs',

	'ACP_HEADERLINK_COLOUR'						=> 'Pour sélectionner une couleur au bouton cliquer sur le champ de saisie du code couleur.',
	'ACP_HEADERLINK_ICON_PLACEHOLDER'			=> 'Code de l’icône FA (Voir la note ci-dessus)',

	'HEADERLINK_CONFIG'							=> 'Paramètres des liens de l’entête',
	'HEADERLINK_SAVED'							=> 'Les paramètres des liens de l’entête ont été sauvegardés',
));
