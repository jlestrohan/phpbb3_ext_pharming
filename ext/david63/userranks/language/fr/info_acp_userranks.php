<?php
/**
*
* @package User Ranks Extension
* @copyright (c) 2015 david63
* @license http://opensource.org/licenses/gpl-license.php GNU Public License
*
*/

/**
* DO NOT CHANGE
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

/// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine
//
// Some characters you may want to copy&paste:
// ’ » “ ” …
//

$lang = array_merge($lang, array(
	'USER_RANKS'						=> 'Rangs',

	'USER_RANKS_BOTS'					=> 'Ignorer les bots',
	'USER_RANKS_BOTS_EXPLAIN'			=> 'N\'inclus pas les Bots dans la liste des membres du rang.',

	'USER_RANKS_ENABLE'					=> 'Activer les rangs utilisateur',
	'USER_RANKS_ENABLE_EXPLAIN'			=> 'Active l\'affichage des rangs utilisateur.',
	'USER_RANKS_EXPLAIN'				=> 'Ici vous pouvez gérer les options d\'affichage des rangs utilisateur.',

	'USER_RANKS_HEADER'					=> 'Affiche le lien des rangs utilisateur dans le header',

	'USER_RANKS_LOG'					=> '<strong>Paramètres des rangs utilisateur mis à jours</strong>',

	'USER_RANKS_MANAGE'					=> 'Gérer les rangs utilisateur',
	'USER_RANKS_MEMBERS'				=> 'Afficher les membres',
	'USER_RANKS_MEMBERS_ADMIN'			=> 'Afficher les membres aux Admins',
	'USER_RANKS_MEMBERS_ADMIN_EXPLAIN'	=> 'Affiche uniquement aux Admins les membres de chaque rang.',
	'USER_RANKS_MEMBERS_EXPLAIN'		=> 'Affiche les membres qui sont dans chaque rang dans la liste des rangs utilisateur à tous les utilisateurs connectés.',

	'USER_RANKS_OPTIONS'				=> 'Options des rangs utilisateur',

	'USER_RANKS_SPECIAL'				=> 'Afficher les rangs spéciaux',
	'USER_RANKS_SPECIAL_ADMIN'			=> 'Afficher les rangs spéciaux aux Admins',
	'USER_RANKS_SPECIAL_ADMIN_EXPLAIN'	=> 'Affiche uniquement aux Admins les rangs spéciaux.',
	'USER_RANKS_SPECIAL_EXPLAIN'		=> 'Affiche les rangs spéciaux dans la liste des rangs utilisateur à tous les utilisateurs connectés.',
));
