<?php

/**
 *
 * @package phpBB Extension - Sort Topics
 * @copyright (c) 2016 kasimi
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

if (!defined('IN_PHPBB'))
{
	exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'SORTTOPICS_TITLE'						=> 'Tri des Sujets',
	'SORTTOPICS_CONFIG'						=> 'Configuration',
	'SORTTOPICS_CONFIG_UPDATED'				=> '<strong>Sort Topics</strong>Extension<br />» Configuration updated',

	'SORTTOPICS_UCP_ENABLED'				=> 'Allow users to globally sort topics by created time',
	'SORTTOPICS_UCP_ENABLED_EXPLAIN'		=> 'Give users the option in the UCP to sort topics by created time in all forums.',

	'SORTTOPICS_SORT_TOPICS_BY'				=> 'Trier les sujets par',
	'SORTTOPICS_SORT_TOPICS_BY_EXPLAIN'		=> 'Une s&eacute;lection autre que “User default” force le tri des sujets de ce forum par la clef sp&eacute;cifi&eacute;e, annulant les effets des r&egrave;glages utilisateur dans son panneau de controle. Celui-ci peux toutefois changer la m&eacute;thode de tri de facon temporaire au bas de chaque page de forum.',
	'SORTTOPICS_SORT_TOPICS_ORDER'			=> 'Ordre de tri',
	'SORTTOPICS_SORT_TOPICS_ORDER_EXPLAIN'	=> "Cette option ne s'applique que lorsque l'option ci-dessus est r&egrave;gl&eacute;e sur “User default”.",
	'SORTTOPICS_APPLY_TO_SUBFORUMS'			=> 'Appliquer le sch&eacute;ma actuel de tri &agrave; tous les sous-forums',
	'SORTTOPICS_APPLY_TO_SUBFORUMS_EXPLAIN'	=> 'Si “Oui“, les pr&eacute;f&eacute;rences de tri ci-dessus seront appliqu&eacute;es &agrave; ce forum et tous les sous-forums enfants.',
	'SORTTOPICS_USER_DEFAULT'				=> 'User default',
));
